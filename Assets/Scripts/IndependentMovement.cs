﻿using UnityEngine;
using System.Collections;

public class IndependentMovement : MonoBehaviour
{

    public Transform[] Wheels;
    // Use this for initialization
    void Start( )
    {

    }

    // Update is called once per frame
    void Update( )
    {
        if ( Input.GetKey( KeyCode.W ) )
        {
            foreach ( Transform c in Wheels )
            {
                c.localEulerAngles += new Vector3( 0, 0, Time.deltaTime * 25f );
            }
        }

    }
}

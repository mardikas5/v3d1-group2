﻿using UnityEngine;
using System.Collections;


public class CameraMovement : MonoBehaviour
{

    // Use this for initialization
    void Start( )
    {

    }

    // Update is called once per frame
    void Update( )
    {
        if ( Input.GetKey( KeyCode.UpArrow ) )
        {
            transform.position += transform.TransformDirection( Vector3.forward );
        }
        if ( Input.GetKey( KeyCode.DownArrow ) )
        {
            transform.position += transform.TransformDirection( -Vector3.forward );
        }
        if ( Input.GetKey( KeyCode.RightArrow ) )
        {
            transform.position += transform.TransformDirection( Vector3.right );
        }
        if ( Input.GetKey( KeyCode.LeftArrow ) )
        {
            transform.position += transform.TransformDirection( Vector3.left );
        }
        transform.eulerAngles += new Vector3( -Input.GetAxis( "Mouse Y" ), Input.GetAxis( "Mouse X" ), 0 );
    }
}

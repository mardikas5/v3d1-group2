﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreateCustomPrimitives : MonoBehaviour
{
    //Type of primitives 
    public enum CustomPrimitive
    {
        Cube,
        Cylinder
    }

    //Selectable enum in Unity Inspector.
    public CustomPrimitive Primitive;

    //Material applied at the start.
    public Material mat;

    private GameObject PrimitiveObject;
    private Mesh myMesh;

    // Use this for initialization
    void Start( )
    {
        CreatePrimitive( Primitive );
    }

    public GameObject CreatePrimitive( CustomPrimitive type )
    {
        PrimitiveObject = new GameObject();

        PrimitiveObject.transform.SetParent( transform );
        PrimitiveObject.transform.localRotation = Quaternion.identity;
        PrimitiveObject.transform.position = transform.position;

        PrimitiveObject.AddComponent<MeshFilter>();
        PrimitiveObject.AddComponent<MeshRenderer>();

        if ( type == CustomPrimitive.Cube )
        {
            PrimitiveObject.name = "Cube";
            PrimitiveObject.GetComponent<MeshFilter>().sharedMesh = CreateCube( 5f );
        }
        if ( type == CustomPrimitive.Cylinder )
        {
            PrimitiveObject.name = "Cylinder";
            PrimitiveObject.GetComponent<MeshFilter>().sharedMesh = CreateCyclinder( 1f, 1f, 15 );
        }
        if ( mat != null )
        {
            PrimitiveObject.GetComponent<MeshRenderer>().material = mat;
        }

        return PrimitiveObject;
    }

    public Mesh CreateCube( float Size )
    {
        float distance = Size;

        Mesh m = new Mesh();

        m.vertices = new Vector3[]
        {
        //Front;
                new Vector3(0, distance, 0),
                new Vector3(distance, distance, 0),
                new Vector3(distance, 0, 0),
                new Vector3(0, 0, 0),
        //Back;
                new Vector3(0, 0, distance),
                new Vector3(distance, 0, distance),
                new Vector3(distance, distance, distance),
                new Vector3(0, distance, distance),

        };

        int[] Indicies = new int[]
        {
            //Front;
            0,1,2,3,
            //Back;
            4,5,6,7,
            //Left
            4,7,0,3,
            //Right
            1,6,5,2,
            //Top
            7,6,1,0,
            //Bottom
            3,2,5,4
        };

    
        m.SetIndices( Indicies, MeshTopology.Quads, 0 );
        m.RecalculateNormals();
        return m;
    }

    public Mesh CreateCyclinder( float radius, float height, int Sections )
    {
        int sections = Sections;

        Mesh m = new Mesh();

        if ( sections < 5 )
        {
            sections = 5;
        }

        float angle = 360f / ( sections - 1 );

        angle *= Mathf.Deg2Rad;

        List<Vector3> vertecies = new List<Vector3>();
        List<int> indicies = new List<int>();

        for ( int i = 0; i < sections - 1; i++ )
        {
            indicies.Add( i + 1 );
            indicies.Add( i + sections );
            indicies.Add( i );

            indicies.Add( i + sections );
            indicies.Add( i + 1 );
            indicies.Add( i + sections + 1 );
        }

        for ( int y = 0; y < 2; y++ )
        {
            for ( int i = 0; i < sections; i++ )
            {
                float X = radius * Mathf.Cos( angle * i );
                float Y = radius * Mathf.Sin( angle * i );
                Vector3 v = new Vector3( X, -y * height, Y );

                vertecies.Add( v );
            }
        }

        //Top
        vertecies.Add( new Vector3( 0, 0, 0 ) );

        for ( int i = 0; i < sections - 1; i++ )
        {
            indicies.Add( i );
            indicies.Add( vertecies.Count - 1 );
            indicies.Add( i + 1 );
        }

        //Bottom
        vertecies.Add( new Vector3( 0, -1 * height, 0 ) );

        for ( int i = 0; i < sections - 1; i++ )
        {
            indicies.Add( i + 1 + sections );
            indicies.Add( vertecies.Count - 1 );
            indicies.Add( i + sections );
        }

        
        m.SetVertices( vertecies );
        m.SetIndices( indicies.ToArray(), MeshTopology.Triangles, 0 );
        m.RecalculateNormals();
        return m;
    }
}

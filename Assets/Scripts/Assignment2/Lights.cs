﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lights : MonoBehaviour
{
    public bool RotateLightAround = true;
    [Tooltip("Toggle UI element to toggle rotating light")]
    public Toggle ListenToToggle; 
    [Tooltip("Slider to set speed of the rotating light")]
    public Slider ListenToSlider; 
    [Tooltip("Set the emission of the emissive material")]
    public Slider EmissionController;
    [Tooltip("Emissive material")]
    public Material EmissiveMaterial;
    [Tooltip("Rotating light transform")]
    Transform RotateLight;
    [Tooltip("The angle to rotate the rotating light every frame")]
    float angle = 15f;
    // Use this for initialization
    void Start( )
    {
        //UI listen events (if bool is toggled etc.)
        if ( ListenToToggle != null )
        {
            ListenToToggle.onValueChanged.AddListener( ( enabled ) => RotateLightAround = enabled );
            ListenToSlider.onValueChanged.AddListener( ( value ) => angle = value * 15f);
            EmissionController.onValueChanged.AddListener( ( value ) => EmissiveMaterial.SetColor( "_EmissionColor", new Color( value, value, value ) ) );
        }
        //Directional
        GameObject Directional = new GameObject();
        Light l = Directional.AddComponent<Light>();
        l.intensity = .8f;
        l.color = new Color( .5f, .5f, .5f );
        Directional.transform.eulerAngles = new Vector3( 45, 0, 0 );
        l.type = LightType.Directional;
        //Spot
        GameObject Spot = new GameObject();
        Spot.transform.position = new Vector3( 2, 1, -5 );
        Light spot = Spot.AddComponent<Light>();
        spot.color = new Color( 0, 1, 0 );
        spot.intensity = .5f;

        spot.type = LightType.Spot;

        //Point
        GameObject Point = new GameObject();
        RotateLight = Point.transform;
        Light point = Point.AddComponent<Light>();
        point.intensity = .5f;
        point.color = new Color( 1, 0, 0 );
        point.type = LightType.Point;
    }

    // Update is called once per frame
    void Update( )
    {
        if ( RotateLightAround )
        {
            RotateLight.transform.RotateAround( new Vector3( 2, 1, -5 ), Vector3.up, angle );
        }
    }
}
